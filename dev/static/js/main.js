$(document).ready(function () {
    svg4everybody({});

    var menu = new MmenuLight( $("#mobile-menu")[0], { title: "Меню" });

    menu.enable( "(max-width: 1300px)" );
    menu.offcanvas();

    $( 'a[href="#mobile-menu"]' ).on("click", function(){
        menu.open();
    });

    $( '.main-nav__item' ).on("click", function(){
        menu.close();
    });

    /*AOS.init({
        duration: "600"
    });*/


    $('[data-fancybox]').fancybox({
        lang: "en",
        i18n: {
            en: {
                CLOSE: "Закрыть",
            }
        }
    });


    $(".slick-1").slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1650,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $(".slick-2").slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1650,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $(".slick-3").slick({
        dots: true,
        arrows: false,
        infinite: false,
        speed: 300,
        slidesToShow: 6,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1650,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });


    // Cache selectors
    var lastId,
        topMenu = $(".main-nav"),
        topMenuHeight = 80,
        // All list items
        menuItems = $(".anchor"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
            var item = $($(this).attr("href"));
            if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
        var href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 1000);
        e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
        // Get container scroll position
        var fromTop = 50;

        // Get id of current scroll item
        var cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
                return this;
        });
        // Get the id of the current element
        cur = cur[cur.length-1];
        var id = cur && cur.length ? cur[0].id : "";

        if (lastId !== id) {
            lastId = id;
            // Set/remove active class
            menuItems
                .parent().removeClass("active")
                .end().filter("[href='#"+id+"']").parent().addClass("active");
        }
    });

    $(document).on("scroll", function (event) {
        $(document).scrollTop() > 100 ? $("header").addClass("fixed") : $("header").removeClass("fixed");
    });

    "use strict"
    let currentQuizPage = 1;
    let cityTypeValue = 0, experienceTypeValue = 0, workTypeValue = 0;

    function setCityTypeValue(cityType) {
        switch (cityType) {
            case 'msk':
                cityTypeValue = 400000;
                break;
            case 'bigCity':
                cityTypeValue = 250000;
                break;
            case 'smallCity':
                cityTypeValue = 150000;
                break;
        }
    }

    function setExperienceTypeValue(experienceType) {
        switch (experienceType) {
            case 'hasEducation':
                experienceTypeValue = 1.2;
                break;
            case 'hasAny':
                experienceTypeValue = 1;
                break;
            case 'none':
                experienceTypeValue = 0.8;
                break;
        }
    }

    function setWorkTypeValue(workType) {
        switch (workType) {
            case 'fullTime':
                workTypeValue = 1.2;
                break;
            case 'halfDay':
                workTypeValue = 1;
                break;
            case 'weekends':
                workTypeValue = 0.8;
                break;
        }
    }

    function openQuizStep(step) {
        setTimeout(() => {
            $(document).find('.form-quiz__section').removeClass('form-quiz__section--show');
            $(document).find('.form-quiz__section-step-' + step).addClass('form-quiz__section--show');

            currentQuizPage = step;
        }, 500);
    }

    function getQuizResult() {
        return cityTypeValue * experienceTypeValue * workTypeValue;
    }

    function sendQuizMessage() {
        const name = $('.js-name').val();
        const city = $('.js-city').val();
        const email = $('.js-email').val();
        const phone = $('.js-phone').val();
        const result = getQuizResult();

        // past needed quiz handler url here
        const quizHandler = 'quiz-handler.php';

        var request = $.post( quizHandler, {
            name: name,
            city: city,
            email: email,
            phone: phone,
            result: result,
        }, 'json')
        .done(function( data ) {
            if(data.send && data.send === 'ok') {
                $(document).find('.form-quiz--show').removeClass('form-quiz--show')
                $(document).find('.form-quiz__section--show').removeClass('form-quiz__section--show')
                $(document).find('.quiz-thanks').addClass('quiz-thanks--show')
            }
        });
    }

    $(document).on('submit', '.js-quiz-form', function (e) {
        e.preventDefault();

        sendQuizMessage();
    })

    $(document).on('click', '.js-quiz-city', function() {
        setCityTypeValue( $(this).val() );
        openQuizStep(2);
    })

    $(document).on('click', '.js-quiz-experience', function() {
        setExperienceTypeValue( $(this).val() );
        openQuizStep(3);
    })

    $(document).on('click', '.js-quiz-time', function() {
        setWorkTypeValue( $(this).val() );
        openQuizStep(4);
    })

    $(document).on('click', '.js-quiz-back', function() {
        openQuizStep(currentQuizPage - 1);
    })
});
